from django.db import models
from django.contrib.auth.models import User


#class User(AbstractUser):
#    USERNAME_FIELD = 'email'
#    email = models.EmailField('email address', unique=True) # changes email to unique and blank to false
#    REQUIRED_FIELDS = [] # removes email from REQUIRED_FIELDS


class Profile(models.Model):
    display_name = models.CharField(max_length=255)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="account/", blank=True)

    def __str__(self):
        return self.user.email

class Invite(models.Model):
    token = models.CharField(max_length=255)
    expiry = models.DateTimeField(blank=True, null=True)
    email = models.CharField(max_length=255)


