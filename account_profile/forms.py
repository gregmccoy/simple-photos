from django import forms

from .models import Profile 

class ProfileForm(forms.Form):
    display_name = forms.CharField(label='Display Name', max_length=200)
    email = forms.CharField(label='Email', max_length=200)
    image = forms.FileField()


class InviteForm(forms.Form):
    name = forms.CharField(label='Name', max_length=200)
    email = forms.CharField(label='Email', max_length=200)
