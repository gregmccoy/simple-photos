from django.shortcuts import render
from django.views.generic import TemplateView, FormView, ListView, View
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.shortcuts import redirect
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.timezone import now

from allauth.account.views import SignupView
from allauth.exceptions import ImmediateHttpResponse
from allauth.account.utils import complete_signup  
from allauth.account import app_settings


from datetime import timedelta
import random
import string

from .forms import ProfileForm, InviteForm
from .models import Profile, Invite

class ProfileView(TemplateView):
    template_name = "account/profile.html"

    def get_context_data(self, **kwargs):
        kwargs["profile"] = Profile.objects.get(user=self.request.user)
        return super().get_context_data(**kwargs)


class ProfileEdit(FormView):
    template_name = "account/edit.html"
    form_class = ProfileForm
    initial = {}
    success_url = "/profile/"

    def form_valid(self, request, *args, **kwargs):
        profile = Profile.objects.get(user=self.request.user)
        images = self.request.FILES.getlist("image")
        profile.image = images[0]
        profile.save()
        return super().form_valid(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        profile = Profile.objects.get(user=self.request.user)
        self.initial = { 
            "display_name": profile.display_name,
            "email": profile.user.email,
            "image": profile.image,
        }
        kwargs["profile"] = profile
        return super().get_context_data(**kwargs)


class ProfileSignupView(SignupView):
    template_name = "account/create.html"
    success_url = "/login/"
    initial = {}
    
    def get(self, request, *args, **kwargs):
        invite = Invite.objects.get(token=kwargs["token"])
        if invite:
            self.initial = { "email": invite.email }
            return super().get(request, *args, **kwargs)
        else:
            return redirect("/404/")

    def form_valid(self, form):
        self.user = form.save(self.request)
        Profile.objects.create(
            user=self.user, 
            display_name=self.request.POST.get("display_name")
        )
        try:
            response = complete_signup(
                self.request, self.user,
                app_settings.EMAIL_VERIFICATION,
                self.get_success_url())
            login(self.request, self.user)
            return response
        except ImmediateHttpResponse as e:
            return e.response


class UserList(ListView):
    model = Profile 
    template_name = "account/users.html"
    context_object_name = "users"


class DeleteUser(View):
    success_url = "/users/"

    def post(self, request, *args, **kwargs):
        profile = Profile.objects.get(id=request.POST["profile"])
        profile.user.delete()
        profile.delete()
        return redirect(self.success_url)


class SendInvitation(FormView):
    template_name = "account/invite.html"
    email_template = "account/email/invite.html"
    form_class = InviteForm
    success_url = "/users/invite/"

    def form_valid(self, form):
        response = super().form_valid(form)
        email = form.cleaned_data.get("email")
        name = form.cleaned_data.get("name")
        owner = Profile.objects.get(user=self.request.user)
        expiry = now() + timedelta(days=3)
        token = ''.join(random.choice(string.ascii_lowercase) for i in range(50))
        invite = Invite.objects.create(email=email, token=token, expiry=expiry)
        url = f"https://{settings.SITE_URL}/create/{invite.token}/"

        if email:
            context = {
                "email": email,
                "name": name,
                "owner": owner.display_name,
                "url": url
            }
            html_message = render_to_string(self.email_template, context=context, request=self.request)
            send_mail(
                f"{owner.display_name} invited you to there photo gallery",
                "",
                settings.DEFAULT_FROM_EMAIL,
                [email],
                html_message=html_message,
            )
            self.kwargs["sent"] = email
        return response


class RequestInvitation(TemplateView):
    template_name="account/invite-sent.html"
    email_template = "account/email/request_invite.html"
    success_url = "/invite-sent/"

    def post(self, request, *args, **kwargs):
        email = request.POST.get("email", "")
        name = request.POST.get("name", "")
        if email and name:
            context = {
                "email": email,
                "name": name,
                "url": f"https://{settings.SITE_URL}/users/invite/"
            }
            html_message = render_to_string(self.email_template, context=context, request=self.request)
            send_mail(
                f"{name} requested an invite to SimplePhotos",
                "",
                settings.DEFAULT_FROM_EMAIL,
                [settings.ADMIN_EMAIL],
                html_message=html_message,
            )
            kwargs["sent"] = email
            return super().get(request, *args, **kwargs)
        return super().post(request, *args, **kwargs)

