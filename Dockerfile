FROM ubuntu

RUN apt-get update
RUN apt-get install -y python3 python3-pip  

EXPOSE 8000

RUN mkdir /app

ADD requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

ADD . /app

WORKDIR /app

#CMD ["./docker-entry.sh"]
CMD ./docker-entry.sh
