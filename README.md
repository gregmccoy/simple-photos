# SimplePhotos

SimplePhotos aims to be a simple private photoshare. It allows for social media style posts with likes and comments. It requires the users to have an account for viewing the posts, and has an invite system.

![](https://i.imgur.com/zoS6536.png)

## Storage Backends
SimplePhotos uses django-storages to allow for using backends such as S3 or Google Cloud Storage for storing photos.<br>
https://django-storages.readthedocs.io/en/latest/

## Email Backends
SimplePhotos uses django-anymail to add support for lots of email backends.<br>
https://anymail.readthedocs.io/en/stable/


## Install

To start clone the repo `https://gitlab.com/gregmccoy/simple-photos.git`

`cd simple-photos` to move into the repo. You'll need to create a `simple_photos/production_settings.py` file and fill out some settings.

```
SECRET_KEY = ""

ALLOWED_HOSTS = ["yourdomain.com"]

DEBUG = False

# Database settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db/db.sqlite3'),
    }
}

# Email invitation requests will come to 
ADMIN_EMAIL = "greg@example.com"

DEFAULT_FROM_EMAIL = "simple_photos@example.com"


# Email backend using mailjet
EMAIL_BACKEND = "anymail.backends.mailjet.EmailBackend"

ANYMAIL = {
    "MAILJET_API_KEY": "",
    "MAILJET_SECRET_KEY": "",
}

# Storage backend settings for S3
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

STATIC_URL = ''

AWS_ACCESS_KEY_ID = ""
AWS_SECRET_ACCESS_KEY = ""
AWS_S3_REGION_NAME = "" 
AWS_S3_ENDPOINT_URL = ""
AWS_STORAGE_BUCKET_NAME = ""
```

### Docker

There is a provided `docker-compose.yaml` that uses sqlite3 by default for easy setup. You'll need to change the `docker-compose.yaml` if you need to use the different database.

In the root of the repo run `docker-compose build` and then `docker-compose up` and the site should be running on `127.0.0.1:8000`

### Create a Super user

While you have the container running use the following command to create a super user `docker-compose exec simple_photos python3 /app/manage.py createsuperuser`


