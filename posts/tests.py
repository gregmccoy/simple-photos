from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile

from account_profile.models import Profile

from .models import Post, PostImage
from .forms import PostForm

def create_profile():
    user = User.objects.create(email="test@test.com", username="tester")
    user.set_password("1234")
    user.save()
    return Profile.objects.create(user=user, display_name="Tester")

def create_post(profile, description="testing"):
    return Post.objects.create(description=description, profile=profile)

# Create your tests here.
class PostFeedTestCase(TestCase):
    def setUp(self):
        self.profile = create_profile()
        self.post1 = create_post(self.profile)
        self.post2 = create_post(self.profile, description="testing 2")
        self.client = Client()
        self.client.login(email="test@test.com", password="1234")

    def test_get_feed(self):
        response = self.client.get("/")
        data = response.context
        self.assertEqual(data.get("profile"), self.profile)
        self.assertEqual(data.get("posts").count(), 2)
        # Newest should always come first, the second post is the newest
        self.assertEqual(data.get("posts").first(), self.post2)


class CreatePostTestCase(TestCase):
    def setUp(self):
        self.profile = create_profile()
        self.client = Client()
        self.client.login(email="test@test.com", password="1234")

    def test_get_auth(self):
        response = self.client.get("/add/")
        self.assertEqual(response.status_code, 403)

    def test_get_create_post(self):
        self.profile.user.is_staff = True
        self.profile.user.save()
        response = self.client.get("/add/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(type(response.context["form"]), PostForm)

    def test_post_create_post(self):
        self.profile.user.is_staff = True
        self.profile.user.save()
        img = SimpleUploadedFile('test.jpg', b'testdata')
        images = PostImage.objects.create(image=img)
        data = { "description": "New Post", "images": images.pk }
        response = self.client.post("/add/", data=data)
        self.assertEqual(Post.objects.all().count(), 1)
        self.assertEqual(Post.objects.all().first().description, "New Post")
        self.assertEqual(Post.objects.all().first().images.first(), images)

