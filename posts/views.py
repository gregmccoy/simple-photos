from django.views.generic import ListView, CreateView, View 
from django.shortcuts import redirect
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import JsonResponse, HttpResponse

from posts.models import Post, PostImage, Comment, Like
from posts.forms import PostForm, PostImageForm
from account_profile.models import Profile

class PostFeed(LoginRequiredMixin, ListView):
    template_name = 'index.html'
    model = Post
    context_object_name = "posts"
    ordering = "-date"

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated:
            kwargs["profile"] = Profile.objects.get(user=self.request.user)
        return super().get_context_data(**kwargs)


class UploadImage(CreateView):
    template_name = 'posts/create.html'
    model = PostImage

    def post(self, request, *args, **kwargs):
        images = self.request.FILES.getlist("image")
        profile = Profile.objects.get(user=self.request.user)
        post_image = PostImage.objects.create(image=images[0])
        return HttpResponse(post_image.pk)


class CreatePost(UserPassesTestMixin, CreateView):
    template_name = 'posts/create.html'
    model = Post
    form_class = PostForm

    def test_func(self):
        return self.request.user.is_staff

    def post(self, request, *args, **kwargs):
        data = self.request.POST
        profile = Profile.objects.get(user=self.request.user)
        images = data.get("images", "").split(",")

        post = Post.objects.create(description=data.get("description"), profile=profile)
        for image in images:
            if image:
                post.images.add(PostImage.objects.get(pk=image))
                post.save()
        return redirect("/") 


class CreateComment(View):

    def post(self, request, *args, **kwargs):
        data = self.request.POST
        profile = Profile.objects.get(user=self.request.user)
        post = Post.objects.get(pk=data.get("post"))
        Comment.objects.create(post=post, profile=profile, text=data.get("text"))
        return redirect("/") 


class CreateLike(View):

    def post(self, request, *args, **kwargs):
        data = self.request.POST
        profile = Profile.objects.get(user=self.request.user)
        post = Post.objects.get(pk=data.get("post"))
        existing = Like.objects.filter(post=post, profile=profile)
        liked = data.get("liked")

        if existing:
            if liked == "false":
                Like.objects.filter(post=post, profile=profile).first().delete()
            return JsonResponse({ "liked": liked }) 
        else:
            if liked == "true":
                Like.objects.create(post=post, profile=profile)
            return JsonResponse({ "liked": liked }) 
