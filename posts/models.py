from django.db import models
from django.core.files.storage import default_storage

from PIL import Image
import io

from account_profile.models import Profile


class Post(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    description = models.TextField()
    link = models.CharField(max_length=300)
    date = models.DateTimeField(blank=True, null=True, auto_now_add=True)

    def liked_by(self):
        profiles = []
        for like in self.like_set.all():
            profiles.append(like.profile)
        return profiles 

    def __str__(self):
        return "{} - {}".format(self.profile.display_name, self.description[:30])


class PostImage(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="images", blank=True, null=True)
    image = models.ImageField(upload_to="posts/", blank=True)

    def save(self, *args, **kwargs):
        super(PostImage, self).save(*args, **kwargs)
        image = Image.open(self.image)
        width, height = image.size

        if width > 1100 or height > 1100:
            if width > height:
                factor = 1100 / width 
            else:
                factor = 1100 / height 

        size = ( int(width * factor), int(height * factor))
        image = image.resize(size, Image.ANTIALIAS)

        in_mem_file = io.BytesIO()
        image.save(in_mem_file, format="JPEG")

        with default_storage.open(self.image.name, 'w') as f:
            f.write(in_mem_file.getvalue())



class Comment(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    date = models.DateTimeField(blank=True, null=True, auto_now_add=True)


class Like(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
