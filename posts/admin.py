from django.contrib import admin
from .models import Post, PostImage, Comment, Like

class PostImageInline(admin.StackedInline):
    model = PostImage 

class PostImageAdmin(admin.ModelAdmin):
    list_display = ('image', 'post')

class PostAdmin(admin.ModelAdmin):
    list_display = ('profile', 'description', 'date')
    inlines = [PostImageInline]

class CommentAdmin(admin.ModelAdmin):
    list_display = ('text', 'profile', 'post')

class LikeAdmin(admin.ModelAdmin):
    list_display = ('profile', 'post')

admin.site.register(PostImage, PostImageAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Like, LikeAdmin)

