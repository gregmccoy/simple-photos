from django.forms import ModelForm

from .models import Post, PostImage

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['description']

class PostImageForm(ModelForm):
    class Meta:
        model = PostImage
        fields = ['post', 'image']
