from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView

from posts.views import PostFeed, CreatePost, CreateComment, CreateLike, \
        UploadImage
from account_profile.views import ProfileView, ProfileEdit, ProfileSignupView,\
        UserList, DeleteUser, SendInvitation, RequestInvitation

urlpatterns = [
    path('', PostFeed.as_view(), name='index'),
    path('add/', CreatePost.as_view(), name='add'),
    path('add/image/', UploadImage.as_view(), name='add-image'),
    path('admin/', admin.site.urls),
    path(
        'login/',
        LoginView.as_view(
            template_name="account/login.html",
        ),
        name="account_login"
    ),
    path('logout/', LogoutView.as_view(), name="account_logout"),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('profile/edit/', ProfileEdit.as_view(), name='profile'),
    path('comment/', CreateComment.as_view(), name='comment'),
    path('like/', CreateLike.as_view(), name='like'),
    path('create/<str:token>/', ProfileSignupView.as_view(), name='account_signup'),
    path('users/', UserList.as_view(), name="account_users"),
    path('users/delete/', DeleteUser.as_view(), name="account_users_delete"),
    path('users/invite/', SendInvitation.as_view(), name="account_users_invite"),
    path('users/invite-request/', RequestInvitation.as_view(), name="account_invite_sent"),

    path('accounts/', include('allauth.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include('rest_api.urls')),
]

if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
